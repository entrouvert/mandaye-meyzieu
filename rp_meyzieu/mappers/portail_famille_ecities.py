
"""
You need to defined 3 variables :

* form_values (defined the login form values):
form_values = {
    'login_url': '/login',
    'post_url': '/login',
    'form_attrs': { 'name': 'form40', },
    'username_field': 'user',
    'password_field': 'pass',
    'post_fields': ['birthdate', 'card_number']
}
login_url, form_attrs, post_fields and username_field are obligatory
* urls (a dictionnary with urls) :
 urls = {
       'associate_url': '/mandaye/associate',
       'connection_url': '/mandaye/sso',
       'login_url': '/mandaye/login'
       }
* mapping
"""

from mandaye.auth.saml2 import END_POINTS_PATH
from rp_meyzieu.filters import portail_famille_ecitiz

from mandaye import config

form_values = {
        'login_url': '/ffaxsslMeyzieu/workflow_url?ECITIZ_ACTIVITY_PATH=Citoyen&ECITIZ_HOME_URL=citoyen.jsp&ECITIZ_PROCESS_ID=guichet&ECITIZ_DECONNECTION_URL=services_list.jsp&ECITIZ_ORG=MEY00',
        'form_attrs': { 'name': 'eCitiz' },
        'post_fields': ['MonIdentifiant_champcalcule6', 'MonMotDePasse_champcalcule9'],
        'username_field': 'MonIdentifiant_champcalcule6',
        'password_field': 'MonMotDePasse_champcalcule9',
        }

urls = {
        'associate_url': '/ffaxsslMeyzieu/mandaye_associate',
        'connection_url': '/mandaye/sso',
        'login_url': '/mandaye/login'
        }

index_url = '/ffaxsslMeyzieu/workflow_url?ECITIZ_ACTIVITY_PATH=Citoyen&ECITIZ_HOME_URL=citoyen.jsp&ECITIZ_PROCESS_ID=guichet&ECITIZ_DECONNECTION_URL=services_list.jsp&ECITIZ_ORG=MEY00'

mapping = [
        {
            'path': r'/$',
            'method': 'GET',
            'redirect': index_url, 
            },  
        {
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {
                'auth': 'login',
                'values': {'condition': 'response.code==302'},
                }
            },  
        {   
            'path': r'/mandaye/sso$',
            'method': 'GET',
            'response': {'auth': 'sso',}
            },  
        {   
            'path': r'/mandaye/slo$',
            'method': 'GET',
            'response': {'auth': 'slo',}
            }, 
        {
            'path': (r'/ffaxsslMeyzieu/$', r'/ffaxsslMeyzieu/workflow_url$'),
            'method': 'GET',
            'on_response': [{
                'content-types': ['text/html'],
                'filter': portail_famille_ecitiz.rewrite_logout_form
                },
                {
                    'content-types': ['text/html'],
                    'filter': portail_famille_ecitiz.add_sso_login_box
                    },
                {
                    'content-types': ['text/html'],
                    'filter': portail_famille_ecitiz.clean_js
                    }
                ],
            },
        {
            'path': r'/ffaxsslMeyzieu/workflow_url$',
            'method': 'POST',
            'on_response': [{
                'content-types': ['text/html'],
                'filter': portail_famille_ecitiz.rewrite_logout_form
                },
                ]
            },
        {
                'path': r'/ffaxsslMeyzieu/mandaye_associate$',
                'method': 'GET',
                'target': '/ffaxsslMeyzieu/workflow_url?ECITIZ_ACTIVITY_PATH=Citoyen&ECITIZ_HOME_URL=citoyen.jsp&ECITIZ_PROCESS_ID=guichet&ECITIZ_DECONNECTION_URL=services_list.jsp&ECITIZ_ORG=MEY00',
                'on_response': [{
                    'filter': portail_famille_ecitiz.associate,
                    'values': {
                        'action': urls['associate_url'],
                        'template': 'portail_famille/associate.html',
                        },
                    },
                    {
                        'content-types': ['text/html'],
                        'filter': portail_famille_ecitiz.clean_js
                        }
                    ]
                },
        {
                'path':  r'/ffaxsslMeyzieu/mandaye_associate$',
                'method': 'POST',
                'response': {
                    'auth': 'associate_submit',
                    'values': {'condition': 'response.code==302'}
                    }
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
                'method': 'POST',
                'response': {'auth': 'single_sign_on_post'}
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout'],
                'method': 'GET',
                'response': {'auth': 'single_logout',}
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout_return'],
                'method': 'GET',
                'response': {
                    'auth': 'single_logout_return',
                    'values': {
                        'next_url': config.template_vars['portal_url']
                        },
                    }
                },
        ]

