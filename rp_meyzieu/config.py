import logging
import os

from ConfigParser import SafeConfigParser
from mandaye.exceptions import ImproperlyConfigured

# get configuration files from :
# 1. default-settings.ini from source code
# 2. os.environ.get('SETTINGS_INI') if it exists
#    else /etc/mandaye-meyzieu/config.ini
#         and then /etc/mandaye-meyzieu/local-config.ini
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SETTINGS_INI = (os.path.join(BASE_DIR, 'default-config.ini'),)
if os.environ.get('SETTINGS_INI'):
    SETTINGS_INI += (os.environ.get('SETTINGS_INI'),)
else:
    ETC_DIR = os.path.join('/', 'etc', 'mandaye-meyzieu')
    SETTINGS_INI += (
        os.path.join(ETC_DIR, 'config.ini'),
        os.path.join(ETC_DIR, 'local-config.ini')
    )

config = SafeConfigParser()
config.read(SETTINGS_INI)

## SQL Backend config
# Database configuration
# dialect+driver://username:password@host:port/database
db_url = config.get('database', 'url')

debug = config.getboolean('debug', 'debug')

# Log configuration
LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,

        'formatters': {
            'console': {
                'format': '%(asctime)s %(levelname)s %(message)s',
                'datefmt': '%H:%M:%S',
                },
            'syslog': {
                'format': 'mandaye-meyzieu(pid=%(process)d) %(name)s %(levelname)s %(uuid)s %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
                }
            },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'console'
                },
            'syslog': {
                'level': 'DEBUG',
                'class': 'entrouvert.logging.handlers.SysLogHandler',
                'formatter': 'syslog',
                'address': '/dev/log'
                },
            },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': False,
                },
            'mandaye': {
                'handlers': ['console', 'syslog'],
                'level': 'INFO',
                'propagate': False,
                },
            'rp_meyzieu': {
                'handlers': ['console', 'syslog'],
                'level': 'INFO',
                'propagate': False,
                },
            },
        }

if config.getboolean('debug', 'log_debug'):
    LOGGING['loggers']['']['level'] = 'DEBUG'
    LOGGING['loggers']['mandaye']['level'] = 'DEBUG'
    LOGGING['loggers']['rp_meyzieu']['level'] = 'DEBUG'

## PATH
# Configuration directory
config_root = config.get('dirs', 'config_root')
# Templates directories
templates_directories = []
if config.get('dirs', 'templates_directories'):
    templates_directories = config.get('dirs', 'templates_directories').split(' ')
templates_directories.append(os.path.join(BASE_DIR, 'templates'))
# Static url
static_url = config.get('dirs', 'static_url')
# Static folder
static_root = config.get('dirs', 'static_root')
# Data dir
data_dir = config.get('dirs', 'data_dir')

# template vars
template_vars = {}
if config.has_section('template_vars'):
    for option in config.options('template_vars'):
        template_vars[option] = config.get('template_vars', option)

# Supported authentification
authentifications = {
        'saml2': 'mandaye.auth.saml2.SAML2Auth'
        }

# sp mappers
mappers = {
        'portail_famille_ecities': 'rp_meyzieu.mappers.portail_famille_ecities',
        }

# Raven Sentry configuration
raven_dsn = config.get('debug', 'sentry_dsn')

# Email notification configuration
email_notification = config.getboolean('email', 'notification')
email_prefix = config.get('email', 'prefix')
smtp_host = config.get('email', 'smtp_host')
smtp_port = config.getint('email', 'smtp_port')
email_from = config.get('email', 'from')
email_to = config.get('email', 'to').split()

# Use long traceback with xtraceback
use_long_trace = config.getboolean('debug', 'use_long_trace')
# Ask Mandaye to auto decompress a response message
# Decompress response only if you load a filter
auto_decompress = config.getboolean('mandaye', 'auto_decompress')
# Ask mandaye to add a toolbar with Mandaye's links
mandaye_toolbar = config.getboolean('mandaye', 'toolbar')
mandaye_offline_toolbar = config.getboolean('mandaye', 'offline_toolbar')
# Authentic 2 auto connection
a2_auto_connection = config.getboolean('mandaye', 'a2_auto_connection')

# Choose storage
# Only mandaye.backends.sql at the moment
if config.get('mandaye', 'storage_backend') == 'sql':
    storage_backend = "mandaye.backends.sql"
else:
    ImproperlyConfigured('Storage backend must be sql')

# Encrypt service provider passwords with a secret
# You should install pycypto to use this feature
encrypt_sp_password = config.getboolean('mandaye', 'encrypt_sp_password')
# Must be a 15, 24, or 32 bytes long
encrypt_secret = config.get('mandaye', 'encrypt_secret')

session_type = config.get('session', 'type')
if session_type not in ('file', 'dbm', 'memory', 'memcached'):
    raise ImproperlyConfigured('Sesssion type %r not supported' % session_type)
if session_type == 'memcached':
    session_type = 'ext:memcached'

# Beaker session configuration
session_opts = {
    'session.type': session_type,
    'session.url': config.get('session', 'url'),
    'session.cookie_expires': config.getboolean('session', 'cookie_expires'),
    'session.timeout': config.getint('session', 'timeout'),
    'session.data_dir': config.get('session', 'data_dir')
}

# Import local config
try:
    from local_config import *
except ImportError, e:
    if not 'local_config' in e.args[0]:
        raise ImproperlyConfigured('Error while importing "local_config.py"')

