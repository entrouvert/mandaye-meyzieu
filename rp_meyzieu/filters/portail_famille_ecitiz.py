# -*- coding: utf-8 -*-

import urllib
import re

from urlparse import parse_qs
from mandaye.log import logger
from mandaye.template import serve_template

def associate(env, values, request, response):
    if response.msg and "<form" in response.msg:
        sub = re.subn(r'<form action="/ffaxsslMeyzieu/workflow_url".*?>',
                '<form action="/ffaxsslMeyzieu/mandaye_associate" method="post" accept-charset="UTF-8">',
                response.msg)
        response.msg = sub[0]
        if sub[1] != 1:
            logger.warning('Filter portail_famille_ecitiz.associate: change form action failed !')
        response.msg = response.msg.replace('<a name="egoSubmitMeConnecter_action133_6T9N"></a>', '')
        response.msg = response.msg.replace('Connexion', 'Association')
        sub = re.subn(r'<input type="submit" name="egoSubmitMeConnecter_action133_6T9N".*?>', '<input type="submit"  title="" class="teamnetButtonValid teamnetButtonValidLeftToRight" style="background: #b53c73;" value="Associer" />',
                response.msg)
        response.msg = sub[0]
        if sub[1] != 1:
            logger.warning('Filter portail_famille_ecitiz.associate: submit replacement failed !')
        qs = parse_qs(env['QUERY_STRING'])
        if qs.has_key('type'):
            values['type'] = qs['type'][0]
        else:
            values['type'] = None
        sub = re.subn(
                re.compile('<div class="egoContainerTableStyle egoContainerTableStyleLeftToRight" id="MessageErreur-champcalcule75-6T9N-Box".*?</div>', re.MULTILINE|re.DOTALL),
                serve_template(values.get('template'), **values),
                response.msg)
        response.msg = sub[0]
        if sub[1] != 1:
            logger.warning('Filter portail_famille_ecitiz.associate: error msg replacement failed !')


    return response

def clean_js(env, values, request, response):
    if response.msg and 'alert("Veuillez, s' in response.msg:
        response.msg = re.sub(r'<script type="text/javascript"> alert\("Veuillez, s\'il vous pl.*?>',
                '', response.msg)
    return response

def add_sso_login_box(env, values, request, response):
    if response.msg and "egoSubmitMeConnecter_action133_6T9N" in response.msg:
        sub = re.subn(r' value="Me connecter"/>',
                ' value="Me connecter"/><a href="/mandaye/sso" class="teamnetButtonValid teamnetButtonValidLeftToRight" style="background: #b53c73;">Me connecter avec mon.meyzieu.fr</a>',
                response.msg)
        response.msg = sub[0]
        if sub[1] != 1:
            logger.warning('Filter portail_famille_ecitiz.sso_login_box failed !')
    return response

def rewrite_logout_form(env, values, request, response):
    if response.msg and 'egoSubmitMeDeconnecter_action220_6T9N' in response.msg:
        response.msg = re.sub(
                re.compile('</form>\n<form enctype="multipart/form-data".*?<input type="submit" name="egoSubmitMeDeconnecter_action220_6T9N".*?</form>', re.MULTILINE|re.DOTALL),
                '<span style="display: block; padding: 0;" class="egoLabelButtonGuidgetStyle egoLabelButtonGuidgetStyleLeftToRight"><a href="/mandaye/slo" style="color: white;" >Me déconnecter</a></span>',
                response.msg)
    return response

