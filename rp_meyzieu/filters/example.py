
from mandaye.template import serve_template

class ReplayFilter:

    @staticmethod
    def associate(env, values, request, response):
        associate = serve_template(values.get('template'), **values)
        response.msg = associate
        return response

