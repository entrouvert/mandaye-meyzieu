import os
from rp_meyzieu import default_config

if os.environ.has_key('MANDAYE_CONFIG_FILES'):
    os.environ['MANDAYE_CONFIG_FILES'] = default_config + ' ' + \
            os.environ['MANDAYE_CONFIG_FILES']
else:
    os.environ['MANDAYE_CONFIG_FILES'] = default_config

from beaker.middleware import SessionMiddleware
from whitenoise import WhiteNoise

import mandaye
from mandaye import config
from mandaye.server import MandayeApp

# production
application = SessionMiddleware(MandayeApp(), config.session_opts)

# development
mandaye_path = os.path.dirname(mandaye.__file__)
application_dev = WhiteNoise(application,
        root=os.path.join(mandaye_path, 'static'),
        prefix=config.static_url)
application_dev.add_files(config.static_root, prefix=config.static_url)
