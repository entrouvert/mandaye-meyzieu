"""
Here you can overload Mandaye default authentification
method like SAML2Auth or AuthForm
"""

from mandaye.auth.authform import AuthForm
from mandaye.auth.saml2 import SAML2Auth

class MyAuthSAML(SAML2Auth):
    """ Overload Mandaye SAML2Auth authentification
    """
    pass

class MyAuth(AuthForm):
    """ Overload Mandaye AuthForm authentification
    """
    pass

