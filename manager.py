#! /usr/bin/python
# -*- coding: utf-8 -*-

""" Script to administrate mandaye server
"""

import os

from optparse import OptionParser
from rp_meyzieu import default_config

def get_cmd_options():
    usage = "usage: %prog --config=/path/to/config.ini --createdb|--upgradedb"
    parser = OptionParser(usage=usage)
    parser.add_option("--config",
            dest="config",
            type="string",
            help="Path of the configuration file"
            )
    parser.add_option("--createdb",
            dest="createdb",
            default=False,
            action="store_true",
            help="Create Mandaye database"
            )
    parser.add_option("--upgradedb",
            dest="upgradedb",
            default=False,
            action="store_true",
            help="Upgrade Mandaye database"
            )
    (options, args) = parser.parse_args()
    return options

def main():
    options = get_cmd_options()

    config_files = [default_config]
    if options.config:
        config_files.append(options.config)
    os.environ['MANDAYE_CONFIG_FILES'] = ' '.join(config_files)

    from mandaye import config
    from mandaye.log import logger
    if options.createdb or options.upgradedb:
        logger.info("Creating or upgrading database...")
        from alembic.config import Config
        from alembic import command
        alembic_cfg = Config(config.alembic_cfg)
        alembic_cfg.set_main_option("script_location", config.alembic_script_path)
        command.upgrade(alembic_cfg, "head")
        logger.info("Database upgraded")

if __name__ == "__main__":
    main()

