#! /usr/bin/env python

'''
   Setup script for rp_meyzieu RP
'''

import os
import subprocess

from setuptools import setup, find_packages
from sys import version

import rp_meyzieu

install_requires=[
        'gunicorn>=0.17',
        'mandaye>=0.10.2',
        'whitenoise>=1.0'
]

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        return version.replace('-','.')
    return rp_meyzieu.__version__

setup(name="rp_meyzieu",
      version=get_version(),
      license="AGPLv3 or later",
      description="rp_meyzieu rp is a Mandaye project, modular reverse proxy to authenticate",
      url="http://dev.entrouvert.org/projects/reverse-proxy/",
      author="Author",
      author_email="author@example.com",
      maintainer="Maintainer",
      maintainer_email="maintainer@exmaple.com",
      scripts=['manager.py', 'server.py'],
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires
)

